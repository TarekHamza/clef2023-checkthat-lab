# Scorers
To evaluate your models, we provide a [scorer](https://gitlab.com/checkthat_lab/clef2023-checkthat-lab/-/blob/main/task5/scorer/evaluate.py). To run the scorer you need to install [Pyterrier](https://pyterrier.readthedocs.io/en/latest/) as shown below:
> pip install python-terrier==0.7.2 <br/>

To evaluate the output of your model which should be in the output format required, please run the below:

> python evaluate.py -g dev_relevance_judgments.txt -p dev_predicted.txt <br/>

where dev_predicted.txt is the output of your model on the dev set, and [dev_relevance_judgments.txt](https://gitlab.com/checkthat_lab/clef2023-checkthat-lab/-/blob/main/task5/data/subtask-5A-arabic/dev_relevance_judgments.txt) is the golden qrels file provided by us.


# Evaluation Metrics

The task is evaluated as a ranking task. The official evaluation measure is P@5 to evaluate how systems are able to retrieve authorities at the top of a short retrieved list. Other measures, such as P@1 and nDCG@5, will also be reported.

