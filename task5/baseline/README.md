# Baselines
Our baseline is a BM25 lexical retrieval model where the query is the rumor text, and the documents collection is the users documents. Each user document is constructed by concatentating his translated profile name and description, and all his translated Twitter lists names and descriptions.

**Lexical baseline results for Task 5 on both train and dev sets**
||P@1|P@5|NDCG@5|Recall@5|Recall@100|
|:----|:----|:----|:----|:----|:----|
|train set | 0.158|0.090|0.081|0.310|0.108|
|dev set|0.267|0.127|0.111|0.347|0.164|
