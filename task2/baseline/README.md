# Baseline


Our baseline is a logistic regression trained on a Sentence-BERT multilingual representation of the data.
As Sentence-BERT encoder we used the 'paraphrase-multilingual-MiniLM-L12-v2' model.

### Baseline results for each language

| Language     | Model                 | Macro F1 | Macro P | Macro R | SUBJ F1 | SUBJ P | SUBJ R | Accuracy |
|:-------------|:----------------------|:---------|:--------|:--------|:--------|:-------|:-------|:---------|
| Arabic       | Baseline (SBERT + LR) | 0.73     | 0.72    | 0.77    | 0.62    | 0.52   | 0.76   | 0.78     |
| Dutch        | Baseline (SBERT + LR) | 0.60     | 0.60    | 0.60    | 0.58    | 0.57   | 0.60   | 0.60     |
| English      | Baseline (SBERT + LR) | 0.74     | 0.74    | 0.74    | 0.74    | 0.75   | 0.73   | 0.74     |
| German       | Baseline (SBERT + LR) | 0.72     | 0.72    | 0.73    | 0.68    | 0.62   | 0.75   | 0.73     |
| Italian      | Baseline (SBERT + LR) | 0.68     | 0.67    | 0.70    | 0.55    | 0.48   | 0.65   | 0.72     |
| Turkish      | Baseline (SBERT + LR) | 0.82     | 0.83    | 0.82    | 0.83    | 0.82   | 0.83   | 0.83     |
| Multilingual | Baseline (SBERT + LR) | 0.73     | 0.73    | 0.73    | 0.75    | 0.71   | 0.79   | 0.73     |