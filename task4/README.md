# Task 4: Factuality of Reporting of News Media


In this task we ask to identify the factuality of reporting at the media level. Given the news article(s) a news outlet (e.g., www.cnn.com), the task asks to predict the factuality of reporting of that news outlet: low, mixed, and high. We offer the task in English


__Table of contents:__
<!-- - [Evaluation Results](#evaluation-results) -->
- [Submission Guidelines](#submission-guidelines)
- [List of Versions](#list-of-versions)
- [Contents of the Directory](#contents-of-the-directory)
- [File Format](#file-format)
	- [Input Data Format](#input-data-format)
	- [Output Data Format](#output-data-format)
- [Format Checkers](#format-checkers)
- [Scorers](#scorers)
- [Evaluation Metrics](#evaluation-metrics)
- [Baselines](#baselines)
- [Credits](#credits)

<!-- ## Evaluation Results

TBA -->

## Submission Guidelines:
- Make sure that you create one account for each team, and submit your runs through one account only.
- Name of the output file have to be 'task4.tsv'; otherwise, you will get an error on the leaderboard.
- You have to zip the tsv, 'zip task4.zip task4.tsv' and submit it through the codalab page.
- It is required to submit the team name and method description for each submission. **The team name here must EXACTLY match that used during CLEF registration**.
- We will keep the leaderboard private till the end of the submission period, hence, results will not be available upon submission. All results will be available after the evaluation period.
- You are allowed to submit max 200 submissions per day for each subtask.
- The last file submitted to the leaderboard will be considered as the final submission.

**Please submit your results on test data here: https://codalab.lisn.upsaclay.fr/competitions/12937**

## List of Versions
- **Task 4-factuality-news-media [2023/05/03]** - Released test set.
- **Task 4-factuality-news-media [2023/05/02]** - Non-English articles removed.
- **Task 4-factuality-news-media [2023/17/03]** - Fixed file format. JSON files are prepared for the media level. Each JSON file contains all articles released for that particular media. Updated baseline scripts.
- **Task 4-factuality-news-media [2023/08/03]** - data for task 4 is released.

## Contents of the Directory

We provide the following files:
- Main folder: [data](./data)
  - Contains data for the task
- Main folder: [baseline](./baseline)<br/>
- 	Contains scripts provided for baseline models of the tasks, including scorer and format-checker
- [README.md](./README.md) <br/>
- 	This file!


## File Format

### Input Data Format

The data will be provided in the format of tsv and JSON:

- The tsv file contains three columns: news source, JSON file path and label. The JSON file path points to a json file, which contains a list of articles. Each item in the list is a JSON object, which contains title, content, label-text (e.g., high) and label (numeric format, e.g., 2).


### Output Data Format

The expected results file is a list of file-id, predicted class label and model name. Each row contains three TAB separated fields:

> id <TAB> class_label <TAB> run_id

Where: <br>
* id: File id for a given news media. <br/>
* class_label: Predicted class label for the news media. <br/>
* run_id: String identifier used by participants. <br/>

**Example:**
> data/task_3B/train_json/thedodo.com.json	0  Model_1<br/>
> data/task_3B/train_json/them.us.json	1  Model_1<br/>
> ... <br/>



## Format Checkers

The checker for the task is located in the [baseline](./baseline) module of the project.
To launch the checker script you need to install packages dependencies found in [requirements.txt](./requirements.txt) using the following:
> pip3 install -r requirements.txt <br/>

The format checker verifies that your generated results files complies with the expected format.
To launch it run:

> python baseline/format_checker.py --pred-files-path <path_to_result_file_1 path_to_result_file_2 ... path_to_result_file_n> <br/>

`--pred-files-path` is to be followed by a single string that contains a space separated list of one or more file paths.

__<path_to_result_file_n>__ is the path to the corresponding file with participants' predictions, which must follow the format, described in the [Output Data Format](#output-data-format) section.

Note that the checker can not verify whether the prediction files you submit contain all tweets, because it does not have access to the corresponding gold file.



## Evaluation Metrics

This is an ordinal classification task. We use mean absolute error as the official measure.

<!-- Submission Link: Coming Soon -->

<!-- Evaluation File task3/evaluation/CLEF_-_CheckThat__Task3ab_-_Evaluation.txt -->

## Scorers
The scorer for the subtask is located in the [baseline](./baseline) module of the project.
To launch the script you need to install packages dependencies found in [requirements.txt](./requirements.txt) using the following:
> pip3 install -r requirements.txt <br/>

Launch the scorer for the subtask as follows:
> python3 baseline/scorer.py --gold-file-path=<path_gold_file> --pred-file-path=<predictions_file> <br/>

The scorer invokes the format checker for the task to verify the output is properly shaped.
It also handles checking if the provided predictions file contains all tweets from the gold one.


## Baselines

The [baselines](./baseline) module currently contains a majority, random and a simple n-gram baseline.

**Baseline Results on the dev set**

|Baseline|MAE|
|:----|:----|
|Mid-label|0.733|
|Majority|0.533|
|Random|0.800|
|ngram|0.392|


To launch the baseline script you need to install packages dependencies found in [requirements.txt](./requirements.txt) using the following:
> pip3 install -r requirements.txt <br/>


To launch the baseline script run the following:
> python3 baseline/baseline.py --train-file-path=<name_of_your_training_file> --dev-file-path=<name_of_your_testing_file> --lang=<language_of_the_subtask> --task=factuality<br/>
```
python3 baseline/baseline.py --train-file-path=data/task_4/task_4_news_media_factuality_train.tsv --dev-file-path=data/task_4/task_4_news_media_factuality_dev.tsv --lang=english --task=factuality
```


## Credits
Please find it on the task website: https://checkthat.gitlab.io/clef2023/task4/
