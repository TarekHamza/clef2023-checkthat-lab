import pdb
import logging
import argparse
import os

import sys

sys.path.append('.')
from format_checker import check_format
from utils import _compute_average_precision, _compute_reciprocal_rank, _compute_precisions
from utils import print_thresholded_metric, print_single_metric
from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import recall_score
from sklearn.metrics import precision_score
from sklearn.metrics import mean_absolute_error

logging.basicConfig(format='%(levelname)s : %(message)s', level=logging.INFO)

MAIN_THRESHOLDS = [1, 3, 5, 10, 20, 50]

<<<<<<< HEAD
=======

>>>>>>> 38b7e682413010e768b84ed40d005bff56f4cd1c
def _read_gold_and_pred(gold_fpath, pred_fpath, task="task-3A"):
    """
    Read gold and predicted data.
    :param gold_fpath: the original annotated gold file, where the last 4th column contains the labels.
    :param pred_fpath: a file with line_number and score at each line.
    :return: {line_number:label} dict; list with (line_number, score) tuples.
    """

    logging.info("Reading gold labels from file {}".format(gold_fpath))

    gold_labels = {}
    with open(gold_fpath, encoding='utf-8') as gold_f:
        next(gold_f)
        for line_res in gold_f:
<<<<<<< HEAD
            if(task=="task-3A"):
                (id, label) = line_res.strip().split('\t')  # process the line from the res file
                gold_labels[str(id)] = label
            elif(task=="task-3B"):
=======
            if (task == "task-3A"):
                (id, label) = line_res.strip().split('\t')  # process the line from the res file
                gold_labels[str(id)] = label
            elif (task == "task-3B"):
>>>>>>> 38b7e682413010e768b84ed40d005bff56f4cd1c
                (source, id, label) = line_res.strip().split('\t')  # process the line from the res file
                gold_labels[str(id)] = label

    logging.info('Reading predicted labels from file {}'.format(pred_fpath))

    line_score = []
    # labels=[]
    with open(pred_fpath) as pred_f:
        next(pred_f)
        for line in pred_f:
            id, pred_label, run_id = line.split('\t')
            id = str(id.strip())
            pred_label = pred_label.strip()

            if id not in gold_labels:
                logging.error('No such id: {} in gold file!'.format(id))
                quit()
            line_score.append((id, pred_label))
            # labels.append(pred_label)

    if len(set(gold_labels).difference([tup[0] for tup in line_score])) != 0:
        logging.error('The predictions do not match the lines from the gold file - missing or extra line_no')
        raise ValueError('The predictions do not match the lines from the gold file - missing or extra line_no')

    return gold_labels, line_score


def evaluate(gold_fpath, pred_fpath, task="task-3A"):
    """
    Evaluates the predicted line rankings w.r.t. a gold file.
    Metrics are: Average Precision, R-Pr, Reciprocal Rank, Precision@N
    :param gold_fpath: the original annotated gold file, where the last 4th column contains the labels.
    :param pred_fpath: a file with line_number at each line, where the list is ordered by check-worthiness.
    """

<<<<<<< HEAD
    #Gold files are differently formatted for 1A and 1B, we need to update this part to serve both
    gold_labels_dict, pred_labels_dict = _read_gold_and_pred(gold_fpath, pred_fpath, task)
    gold_labels=[]
    pred_labels=[]
=======
    # Gold files are differently formatted for 1A and 1B, we need to update this part to serve both
    gold_labels_dict, pred_labels_dict = _read_gold_and_pred(gold_fpath, pred_fpath, task)
    gold_labels = []
    pred_labels = []
>>>>>>> 38b7e682413010e768b84ed40d005bff56f4cd1c
    for t_id, label in gold_labels_dict.items():
        gold_labels.append(int(label))
    for t_id, label in pred_labels_dict:
        pred_labels.append(int(label))

    # Calculate Metrics

    # acc = accuracy_score(gold_labels, pred_labels)
    # precision = precision_score(gold_labels, pred_labels, pos_label='Yes',average='binary')
    # recall = recall_score(gold_labels, pred_labels, pos_label='Yes',average='binary')
    # f1 = f1_score(gold_labels, pred_labels, pos_label='Yes',average='binary')
    mae = mean_absolute_error(gold_labels, pred_labels)

    return mae


def validate_files(pred_file):
    if not check_format(pred_file):
        logging.error('Bad format for pred file {}. Cannot score.'.format(pred_file))
        return False
    return True


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--gold-file-path", "-g", required=True, type=str,
                        help="Path to file with gold annotations.")

    parser.add_argument("--pred-file-path", "-p", required=True, type=str,
                        help="Path to file with predict class per tweet.")
    parser.add_argument("--subtask", "-a", required=True,
                        choices=['task-3A','task-3B'],
                        help="The subtask you want to score its runs.")
    args = parser.parse_args()

    pred_file = args.pred_file_path
    gold_file = args.gold_file_path
    subtask = args.subtask

    if validate_files(pred_file):
        logging.info(f"Started evaluating results ...")
        overall_precisions = [0.0] * len(MAIN_THRESHOLDS)

        mae = evaluate(gold_file, pred_file, subtask)
        print("mae:{}".format(mae))