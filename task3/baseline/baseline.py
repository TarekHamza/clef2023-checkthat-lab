import pandas as pd
import random
import logging
import argparse
from os.path import join, dirname, basename
from sklearn.dummy import DummyClassifier
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.svm import SVC

import sys
import json
sys.path.append('.')

from scorer import evaluate
from format_checker import check_format

random.seed(0)
ROOT_DIR = dirname(dirname(__file__))

logging.basicConfig(format='%(levelname)s : %(message)s', level=logging.INFO)

def read_content(data, task="task-3A"):
    labels=[]
    content=[]
    if(task=="task-3A"):
        for index, row in data.iterrows():
            json_file=row["json_file_path"]
            with open(json_file) as f:
                file_contents = json.load(f)
            text=file_contents["title"]+" "+file_contents["content"]
            content.append(text)
            label=row["label"]
            labels.append(label)
    elif(task=="task-3B"):
        for index, row in data.iterrows():
            json_file=row["json_file_path"]
            with open(json_file) as f:
                file_contents = json.load(f)
            articles=file_contents["articles"]
            #print(articles)
            article_text=""
            for article in articles:
                text=article["title"]+" "+article["content"]
                article_text=article_text+"\n"+text
            content.append(article_text.strip())
            label=row["label"]
            labels.append(label)
    # print("Num of instances: {}, {}".format(len(content),len(labels)))
    return content, labels


def run_midlabel_baseline(data_fpath, test_fpath, results_fpath, task="task-3A"):

    train_df = pd.read_csv(data_fpath, dtype=object, sep='\t')
    test_df = pd.read_csv(test_fpath, dtype=object, sep='\t')
    pipeline = DummyClassifier(strategy="constant", constant="1")

    train_content, train_labels=read_content(train_df, task)
    test_content, test_labels = read_content(test_df, task)
    pipeline.fit(train_content, train_labels)

    with open(results_fpath, "w") as results_file:
        predicted_labels = pipeline.predict(test_content)

        results_file.write("id\tclass_label\trun_id\n")

        for i, line in test_df.iterrows():
            label = predicted_labels[i]

            results_file.write("{}\t{}\t{}\n".format(line['json_file_path'], label, "midlabel"))


def run_majority_baseline(data_fpath, test_fpath, results_fpath, task="task-3A"):

    train_df = pd.read_csv(data_fpath, dtype=object, sep='\t')
    test_df = pd.read_csv(test_fpath, dtype=object, sep='\t')

    pipeline = DummyClassifier(strategy="most_frequent")

    train_content, train_labels=read_content(train_df, task)
    test_content, test_labels = read_content(test_df, task)
    pipeline.fit(train_content, train_labels)

    with open(results_fpath, "w") as results_file:
        predicted_labels = pipeline.predict(test_content)

        results_file.write("id\tclass_label\trun_id\n")

        for i, line in test_df.iterrows():
            label = predicted_labels[i]

            results_file.write("{}\t{}\t{}\n".format(line['json_file_path'], label, "majority"))


def run_random_baseline(data_fpath, results_fpath, task="task-3A"):
    gold_df = pd.read_csv(data_fpath,  dtype=object, sep='\t')

    data_content, data_labels=read_content(gold_df,task)
    # test_content, test_labels = read_content(test_df)

    #Avoid leaking test set label distribution
    data_labels = ["0","1","2"]

    # label_list=train_labels
    # print("label list:{}".format(label_list))
    with open(results_fpath, "w") as results_file:
        results_file.write("id\tclass_label\trun_id\n")
        for i, line in gold_df.iterrows():
            results_file.write('{}\t{}\t{}\n'.format(line['json_file_path'],random.choice(data_labels), "random"))


def run_ngram_baseline(train_fpath, test_fpath, results_fpath, task="task-3A"):
    train_df = pd.read_csv(train_fpath, dtype=object, sep='\t')
    test_df = pd.read_csv(test_fpath, dtype=object, sep='\t')

    train_content, train_labels=read_content(train_df, task)
    test_content, test_labels = read_content(test_df, task)
    # print("text: ",train_content[0])
    pipeline = Pipeline([
        ('ngrams', TfidfVectorizer(ngram_range=(1, 1),lowercase=True,use_idf=True,max_df=0.95, min_df=3,max_features=2000)),
        ('clf', SVC(C=1, gamma='scale', kernel='linear', random_state=0))
    ])
    pipeline.fit(train_content, train_labels)

    with open(results_fpath, "w") as results_file:
        predicted_labels = pipeline.predict(test_content)
        results_file.write("id\tclass_label\trun_id\n")
        for i, line in test_df.iterrows():
            label = predicted_labels[i]
            results_file.write("{}\t{}\t{}\n".format(line['json_file_path'], label, "ngram"))


def run_baselines(train_fpath, test_fpath, lang, task="task-3A"):
    ## mid-label
    midlabel_baseline_fpath = join(ROOT_DIR,
                                 f'data/midlabel_baseline_{basename(test_fpath)}')
    run_midlabel_baseline(train_fpath, test_fpath, midlabel_baseline_fpath, task=task)

    if check_format(midlabel_baseline_fpath):
        mae = evaluate(test_fpath, midlabel_baseline_fpath, task=task)
        logging.info(f"Midlabel Baseline for {lang} MAE : {mae}")

    ## majority
    majority_baseline_fpath = join(ROOT_DIR,
                                 f'data/majority_baseline_{basename(test_fpath)}')
    run_majority_baseline(train_fpath, test_fpath, majority_baseline_fpath, task=task)
    if check_format(majority_baseline_fpath):
        mae = evaluate(test_fpath, majority_baseline_fpath, task=task)
        logging.info(f"Majority Baseline for {lang} MAE : {mae}")

    ## random
    random_baseline_fpath = join(ROOT_DIR, f'data/random_baseline_{basename(test_fpath)}')
    run_random_baseline(test_fpath, random_baseline_fpath, task=task)

    if check_format(random_baseline_fpath):
        mae = evaluate(test_fpath, random_baseline_fpath, task=task)
        logging.info(f"Random Baseline for {lang} MAE : {mae}")

    ## ngram
    ngram_baseline_fpath = join(ROOT_DIR, f'data/ngram_baseline_{basename(test_fpath)}')
    run_ngram_baseline(train_fpath, test_fpath, ngram_baseline_fpath, task=task)
    if check_format(ngram_baseline_fpath):
        mae = evaluate(test_fpath, ngram_baseline_fpath, task=task)
        logging.info(f"Ngram Baseline for {lang} MAE : {mae}")

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--train-file-path", "-t", required=True, type=str,
                        help="The absolute path to the training data")
    parser.add_argument("--dev-file-path", "-d", required=True, type=str,
                        help="The absolute path to the dev data")
    parser.add_argument("--lang", "-l", required=True, type=str,
                        choices=['english'],
                        help="The language of the subtask")
    parser.add_argument("--task", "-s", required=True, type=str,
                        choices=['task-3A','task-3B'],
                        help="The name of the subtask")

    args = parser.parse_args()
    run_baselines(args.train_file_path, args.dev_file_path, args.lang, args.task)